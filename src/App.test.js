import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import AssetTable from './components/AssetTable';
import EntryTable from './components/EntryTable';
import Menu from './components/Menu';
import SpaceViewer from './components/SpaceViewer';
import TabView from './components/TabView';
import Username from './components/Username';
import { act } from 'react-dom/test-utils';


//Testing the App.js
it('<App> renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

//Testing the SpaceView.js
it('<SpaceViewer> renders without crashing' , () => {
  const card = document.createElement('Card');
  ReactDOM.render(<SpaceViewer />, card);
  ReactDOM.unmountComponentAtNode(card);
});

//Testing the EntryTable.js
it('<EntryTable> renders without crashing' , () => {
  const paper = document.createElement('Paper');
  ReactDOM.render(<EntryTable />, paper);
  ReactDOM.unmountComponentAtNode(paper);
});

//Testing the TabView.js
it('<TabView> renders without crashing' , () => {
  const div = document.createElement('div');
  ReactDOM.render(<TabView />, div);
  ReactDOM.unmountComponentAtNode(div);
});

//Testing the AssetTable.js
it('<AssetTable> renders without crashing' , () => {
  const paper = document.createElement('Paper');
  ReactDOM.render(<Username />, paper);
  ReactDOM.unmountComponentAtNode(paper);
});

//Testing the Username.js
it('<Username> renders without crashing' , () => {
  const span = document.createElement('span');
  ReactDOM.render(<AssetTable />, span);
  ReactDOM.unmountComponentAtNode(span);
});
