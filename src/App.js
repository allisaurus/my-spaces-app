import React from 'react';
import Menu from './components/Menu';
import SpaceViewer from './components/SpaceViewer';

// flexbox used to have two columns, menu and view
// https://css-tricks.com/snippets/css/a-guide-to-flexbox/
const appStyle = {
    container: {
        display: 'flex',
        alignItems: 'flex-start'
    },
    menu: {
        flexGrow: 2
    },
    viewer: {
        flexGrow: 5,
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            spaceId: null
        };

        // this binds the "this" in changeSpaceView to the App component
        // so we update the state here even though it's called by Menu
        this.changeSpaceView = this.changeSpaceView.bind(this);
    }

    componentDidMount() {
        document.title = 'you clicked ${this.state.spaceId}';
    }

    componentDidUpdate() {
        document.title = 'you clicked ${this.state.spaceId}';
    }

    // this is a callback method that is going to be used by the Menu
    // when the user wants to see a new space
    changeSpaceView(spaceId) {
        this.setState({ spaceId: spaceId });
    }

    render() {
        // this is the spaceId that we want to display in the SpaceViewer
        let spaceId = this.state.spaceId;

        return (
            <div style={appStyle.container}>
                <div style={appStyle.menu}>
                    <Menu onSpaceChange={this.changeSpaceView} />
                </div>
                <div style={appStyle.viewer}>
                    <SpaceViewer spaceId={spaceId} />
                </div>
            </div>
        );
    }

}

export default App;
