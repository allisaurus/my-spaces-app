import React from 'react'
import PropTypes from 'prop-types';
import EntryTable from './EntryTable';
import AssetTable from './AssetTable';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

// From material-ui docs
// https://material-ui.com/components/tabs
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

// From material-ui docs
// https://material-ui.com/components/tabs
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

// From material-ui docs
// https://material-ui.com/components/tabs
function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

class TabView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            spaceId: this.props.spaceId,
            selectedTab: 0
        };

        this.handleTabChange = this.handleTabChange.bind(this);
    }

    // Changing properties should change the state
    componentWillReceiveProps(nextProps) {
        if (nextProps.spaceId !== this.state.spaceId) {
            this.setState({ spaceId: nextProps.spaceId });
        }
    }

    handleTabChange(event, newValue) {
        this.setState({ selectedTab: newValue });
    }
    render() {
        const spaceId = this.state.spaceId;
        const selectedTab = this.state.selectedTab;

        return (
            <div>
                <AppBar position="static">
                    <Tabs value={selectedTab} onChange={this.handleTabChange} aria-label="Simple tabs example">
                        <Tab label="Entries" {...a11yProps(0)} />
                        <Tab label="Assets" {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                <TabPanel value={selectedTab} index={0}>
                    <EntryTable spaceId={spaceId} />
                </TabPanel>
                <TabPanel value={selectedTab} index={1}>
                    <AssetTable spaceId={spaceId} />
                </TabPanel>
            </div>
        );
    }
}

export default TabView;


