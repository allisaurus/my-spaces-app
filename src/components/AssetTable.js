import React from 'react';
import axios from 'axios';
import Username from './Username';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const tableStyle = {
    table: {
        border: '2px solid black',
        width: '100%'
    },
    headRow: {
        backgroundColor: 'lightgray'
    }

};

function createData(title, contentType, fileName, createdBy, updatedBy, updatedAt) {
    return { title, contentType, fileName, createdBy, updatedBy, updatedAt };
}

function formatDate(dateString) {
    return new Date(dateString).toLocaleDateString("en-US");
}

class EntryTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            spaceId: this.props.spaceId,
            rows: []
        };
    };

    componentWillMount() {
        this.updateSpaceInView(this.state.spaceId)
    }

    // Changing properties should change the state
    componentWillReceiveProps(nextProps) {
        if (nextProps.spaceId !== this.state.spaceId) {
            this.updateSpaceInView(nextProps.spaceId)
        }
    }

    updateSpaceInView(spaceId) {
        if (!spaceId) return; // spaceId will be null on first load

        this.setState({ spaceId: spaceId });

        // Call API to update view with the new space
        axios.get('/space/' + spaceId + "/assets").then(res => {
            const assets = res.data;

            const rows = assets.map((e) => {
                return createData(e.fields.title, e.fields.contentType, e.fields.fileName, e.sys.createdBy, e.sys.updatedBy, e.sys.updatedAt)
            });

            this.setState({ rows: rows });
        }).catch(res => {
            alert('Unable to load entries for space with id ' + spaceId + '. Please check your internet connection');
        });
    }


    render() {
        const rows = this.state.rows;

        return (
            <Paper>
                <Table style={tableStyle.table}>
                    <TableHead>
                        <TableRow style={tableStyle.headRow}>
                            <TableCell>Title</TableCell>
                            <TableCell>Content Type</TableCell>
                            <TableCell>File Name</TableCell>
                            <TableCell>Created By</TableCell>
                            <TableCell>Updated By</TableCell>
                            <TableCell>Last Updated</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.title}>
                                <TableCell component="th" scope="row">
                                    {row.title}
                                </TableCell>
                                <TableCell>{row.contentType}</TableCell>
                                <TableCell>{row.fileName}</TableCell>
                                <TableCell><Username userId={row.createdBy} /></TableCell>
                                <TableCell><Username userId={row.updatedBy} /></TableCell>
                                <TableCell align="right">{formatDate(row.updatedAt)}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>

        );
    }
}

export default EntryTable;