import React from 'react'
import axios from 'axios';

class Username extends React.Component {
    // simple cache so API isn't called all the time
    static CACHE = {};

    constructor(props) {
        super(props);

        this.state = {
            userId: this.props.userId,
            username: null
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userId !== this.state.userId) {
            this.updateUsername(nextProps.userId)
        }
    }

    componentWillMount() {
        this.updateUsername(this.state.userId)
    }

    updateUsername(userId) {
        if (!userId) return;

        if (userId in Username.CACHE) {
            this.setState({ username: Username.CACHE[userId] });
        } else {
            axios.get('users/' + userId).then(res => {
                const user = res.data;
                Username.CACHE[userId] = user.fields.name;
                this.setState({ username: user.fields.name });
            }).catch(res => {
                this.setState({ username: 'Unknown' });
                alert('Unable to load user with id ' + userId + '. Please check your internet connection');
            });
        }
    }

    render() {
        return (
            <span>{this.state.username}</span>
        );
    }

}

export default Username;
