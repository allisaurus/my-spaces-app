import React from 'react';
import axios from 'axios';
import TabView from './TabView';
import Username from './Username';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';


const viewerStyle = {
    subtitle: {
        fontSize: '0.5em',
        verticalAlighn: 'middle',
        marginLeft: '20px'
    },
    textarea: {
        border: '2px solid black',
        padding: '10px',
        minHeight: '150px',
        margin: '20px',
        width: '40%'
    }
}

class SpaceViewer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            spaceId: this.props.spaceId,
            title: '',
            description: '',
            createdBy: '',
        };
    }

    // https://stackoverflow.com/questions/32414308/updating-state-on-props-change-in-react-form
    componentWillReceiveProps(nextProps) {
        if (nextProps.spaceId !== this.state.spaceId) {
            this.updateSpaceInView(nextProps.spaceId)
        }
    }

    componentWillMount() {
        this.updateSpaceInView(this.state.spaceId)
    }

    // this function is called both when the component
    // loads and when the spaceId property changes
    updateSpaceInView(spaceId) {
        if (!spaceId) return;    // spaceId will be null on first load

        this.setState({ spaceId: spaceId });

        // call api to update view with the new space
        axios.get('/space/' + spaceId).then(res => {
            const space = res.data;

            // This state will be used in the render function
            this.setState({
                title: space.fields.title,
                description: space.fields.description,
                createdBy: space.sys.createdBy
            });

        }).catch(res => {
            alert('Unable to load space with id ' + spaceId + '. Please check your internet connection');
        });
    }

    render() {
        const spaceId = this.state.spaceId;
        const title = this.state.title;
        const description = this.state.description;
        const createdBy = this.state.createdBy;

        return (

            <Card>
                <CardContent>
                    <h1>
                        {title}
                        <em style={viewerStyle.subtitle}>created by <Username userId={createdBy} /></em>
                    </h1>
                    <Card style={viewerStyle.textarea}>
                        {description}
                    </Card>
                    <TabView spaceId={spaceId} />
                </CardContent>
            </Card>
        );
    }
}

export default SpaceViewer;