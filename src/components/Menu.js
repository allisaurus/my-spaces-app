import React from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';

const menuStyle = {
    menu: {
        width: '100%',
        padding: 0
    },
    item: {
        listStyle: 'none'
    },
    button: {
        width: '90%',
        marginLeft: '5%'
    }
}

class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            menuItems: []
        };
    }

    componentWillMount() {
        // TODO load this list from api
        axios.get('/space').then(res => {
            const spaceItems = res.data.items;

            // convert the returned space items into just {title, id}
            const newMenuItems = spaceItems.map((i) => ({ title: i.fields.title, id: i.sys.id }));

            // save the spaces in the state so they can be rendered as buttons
            this.setState({ menuItems: newMenuItems });

            // If there are spaces, display the first one
            if (newMenuItems.length > 0) {
                this.handleChangeSpace(newMenuItems[0].id);
            }
        }).catch(res => {
            this.setState({ menuItems: [] });
            alert('Unable to load spaces. Please check your internet connection');
        });
    }

    handleChangeSpace(item) {
        // use the callback from the parent component
        // so it canupdate the spaceId in SpaceViewer
        this.props.onSpaceChange(item)
    }

    render() {
        const menuItems = this.state.menuItems;
        return (
            <ul style={menuStyle.menu}>
                {menuItems.map((i) => {
                    return (
                        <li key={i.id} style={menuStyle.item} onClick={() => this.handleChangeSpace(i.id)}>
                            <Button variant="contained" style={menuStyle.button}>
                                {i.title}
                            </Button>
                        </li>
                    );
                })}
            </ul>
        );
    }
}

export default Menu;